from PyQt5.QtCore import QSortFilterProxyModel, pyqtSlot
from PyQt5.QtSql import QSqlTableModel
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QHeaderView

from widgets.database import MyConnection


class ReportTableView(QTableView):
    table = None

    def __init__(self, parent):
        super(ReportTableView, self).__init__(parent)
        # Database and model
        self.db = MyConnection.db
        self.model = QSqlTableModel(db=self.db)
        self.proxy = QSortFilterProxyModel(self.model)

        # TableView settings
        self.setSortingEnabled(True)
        self.verticalHeader().setVisible(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        # TableModel settings
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)

        # ProxyModel settings
        self.proxy.setFilterKeyColumn(-1)

    @pyqtSlot(str)
    def change_model(self, text):
        self.table = text
        self.update_model(self.table)

    @pyqtSlot(int)
    def update_model(self, i=1000):
        if isinstance(i, int):
            if i not in [3, 1000]:
                return
        self.model.setTable(self.table)
        self.model.select()
        self.proxy.setSourceModel(self.model)
        self.setModel(self.proxy)
