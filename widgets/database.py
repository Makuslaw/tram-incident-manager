import sys
from PyQt5.QtSql import QSqlDatabase
from PyQt5.QtWidgets import QMessageBox


class MyConnection:
    db = QSqlDatabase('QMYSQL')
    db.setHostName("localhost")
    db.setDatabaseName("tram_incident_manager")
    db.setUserName("tim")
    db.setPassword("12345")
    db.open()
    if db.lastError().type():
        message = QMessageBox(text="Nie udało się nazwiązać połączenia z bazą danych.")
        message.exec_()
        sys.exit()
