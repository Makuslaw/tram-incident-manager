from PyQt5.QtSql import QSqlQuery
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtCore import pyqtSlot, pyqtSignal

from widgets.database import MyConnection


class TableComboBox(QComboBox):
    table_changed = pyqtSignal(str)
    db = MyConnection.db

    def __init__(self, parent):
        super(TableComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.addItems(self.db.tables())

        # signals
        self.currentTextChanged.connect(self.text_changed)

    def text_changed(self):
        self.table_changed.emit(self.currentText())


class ColumnComboBox(QComboBox):
    column_changed = pyqtSignal(str)
    db = MyConnection.db

    def __init__(self, parent):
        super(ColumnComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT `COLUMN_NAME` '
                    'FROM `INFORMATION_SCHEMA`.`COLUMNS` '
                    'WHERE `TABLE_SCHEMA`=\'tram_incident_manager\' '
                    f'AND `TABLE_NAME`=\'{self.db.tables()[0]}\'')
        while query.next():
            self.addItem(query.value(0))

        # signals
        self.currentTextChanged.connect(self.text_changed)

    @pyqtSlot(str)
    def update_data(self, table):
        self.clear()
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT `COLUMN_NAME` '
                    'FROM `INFORMATION_SCHEMA`.`COLUMNS` '
                    'WHERE `TABLE_SCHEMA`=\'tram_incident_manager\' '
                    f'AND `TABLE_NAME`=\'{table}\'')
        while query.next():
            self.addItem(query.value(0))

    def text_changed(self):
        self.column_changed.emit(str(self.currentIndex() - 1))


class AssignComboBox(QComboBox):
    assign_column_changed = pyqtSignal(str)
    db = MyConnection.db

    def __init__(self, parent):
        super(AssignComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT `COLUMN_NAME` '
                    'FROM `INFORMATION_SCHEMA`.`COLUMNS` '
                    'WHERE `TABLE_SCHEMA`=\'tram_incident_manager\' '
                    f'AND `TABLE_NAME`=\'PRZYDZIAL_INFO\'')
        while query.next():
            self.addItem(query.value(0))

        # signals
        self.currentTextChanged.connect(self.text_changed)

    def text_changed(self):
        self.assign_column_changed.emit(str(self.currentIndex() - 1))


class IncidentColumnComboBox(QComboBox):
    incident_column_changed = pyqtSignal(str)
    db = MyConnection.db

    def __init__(self, parent):
        super(IncidentColumnComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT `COLUMN_NAME` '
                    'FROM `INFORMATION_SCHEMA`.`COLUMNS` '
                    'WHERE `TABLE_SCHEMA`=\'tram_incident_manager\' '
                    f'AND `TABLE_NAME`=\'USTERKA\'')
        while query.next():
            self.addItem(query.value(0))

        # signals
        self.currentTextChanged.connect(self.text_changed)

    def text_changed(self):
        self.incident_column_changed.emit(str(self.currentIndex() - 1))


class IncidentTypeComboBox(QComboBox):
    db = MyConnection.db

    def __init__(self, parent):
        super(IncidentTypeComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.update_data()

    @pyqtSlot(int)
    def update_data(self, i=1000):
        if i not in [2, 1000]:
            return
        self.clear()
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT TYP_USTERKI FROM TYP_USTERKI')
        while query.next():
            self.addItem(query.value(0))


class HomeStationComboBox(QComboBox):
    db = MyConnection.db

    def __init__(self, parent):
        super(HomeStationComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.update_data()

    @pyqtSlot(int)
    def update_data(self, i=1000):
        if i not in [2, 1000]:
            return
        self.clear()
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT ID_ZAJEZD FROM ZAJEZDNIA')
        while query.next():
            self.addItem(str(query.value(0)))


class StatusComboBox(QComboBox):
    db = MyConnection.db

    def __init__(self, parent):
        super(StatusComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.update_data()

    @pyqtSlot(int)
    def update_data(self, i=1000):
        if (i == 1000) or \
                (i == 2 and self.objectName() == 'comboBox_7') or \
                (i == 4 and self.objectName() in ['comboBox_8', 'comboBox_9', 'comboBox_10']):
            pass
        else:
            return
        self.clear()
        self.addItem('')
        query = QSqlQuery(self.db)
        query.exec_('SELECT STATUS FROM STATUS')
        while query.next():
            self.addItem(query.value(0))


class ReportComboBox(QComboBox):
    db = MyConnection.db
    report_changed = pyqtSignal(str)

    display_names = ['Usterki bez pracowników',
                     'Liczba usterek dla każdego modelu',
                     'Liczba usterek o danym statusie',
                     'Liczba usterek dla modelu i typu']

    view_names = ['USTERKI_BEZ_PRAC',
                  'USTERKI_DLA_MODELU',
                  'USTERKI_STATUSY',
                  'USTERKI_MODEL_TYP']

    def __init__(self, parent):
        super(ReportComboBox, self).__init__(parent)
        # initial population of combobox contents
        self.addItems(self.display_names)
        self.currentTextChanged.connect(self.text_changed)

    def text_changed(self):
        self.report_changed.emit(self.view_names[self.currentIndex()])
