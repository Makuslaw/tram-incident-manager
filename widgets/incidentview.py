from PyQt5.QtCore import QSortFilterProxyModel, pyqtSlot, QRegExp, Qt
from PyQt5.QtSql import QSqlTableModel
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QHeaderView, QMessageBox

from widgets.database import MyConnection
from widgets.editdialog import EditDialog


class IncidentTableView(QTableView):
    tableView_prac = None
    tableView_uster = None
    table = None

    def __init__(self, parent):
        super(IncidentTableView, self).__init__(parent)
        # Database and model
        self.db = MyConnection.db
        self.model = QSqlTableModel(db=self.db)
        self.proxy = QSortFilterProxyModel(self.model)

        # TableView settings
        self.setSortingEnabled(True)
        self.verticalHeader().setVisible(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        # TableModel settings
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)

        # ProxyModel settings
        self.proxy.setFilterKeyColumn(-1)

        # Signal to slot
        self.doubleClicked.connect(self.edit_incident)

    @pyqtSlot(int)
    def update_model(self, i=1000):
        if i not in [2, 1000]:
            return
        self.model.setTable(self.table)
        self.model.select()
        self.proxy.setSourceModel(self.model)
        self.setModel(self.proxy)

    @pyqtSlot(str)
    def filter_changed(self, text):
        sender = self.sender().objectName()
        if sender == 'lineEdit_5':
            search = QRegExp(text, cs=Qt.CaseInsensitive)
            self.proxy.setFilterRegExp(search)
        elif sender == 'comboBox_15':
            self.proxy.setFilterKeyColumn(int(text))

    @pyqtSlot()
    def add_incident(self):
        typ_uster = ""
        id_zajezd = ""
        status = ""
        id_tram = 0
        data = None
        for child in self.parent().children():
            if child.objectName() == 'comboBox_5':
                typ_uster = child.currentText()
            if child.objectName() == 'comboBox_6':
                id_zajezd = child.currentText()
            if child.objectName() == 'comboBox_7':
                status = child.currentText()
            if child.objectName() == 'tableView_6':
                real_index = child.proxy.mapToSource(child.currentIndex())
                real_row = real_index.row()
                real_record = child.model.record(real_row)
                id_tram = real_record.value('ID_TRAM')
            if child.objectName() == 'calendarWidget':
                data = child.selectedDate()
                data = data.toString("yyyy-MM-dd")

        if typ_uster == "" or \
                id_zajezd == "" or \
                status == "" or \
                id_tram == 0:
            message = QMessageBox()
            message.setText("Wypełnij wszystkie pola!")
            message.exec_()
            return

        record = self.model.record()
        record.setValue('ID_TRAM', id_tram)
        record.setValue('TYP', typ_uster)
        record.setValue('ID_ZAJEZD', int(id_zajezd))
        record.setValue('STATUS', status)
        record.setValue('DATA', data)
        self.model.insertRecord(-1, record)
        self.model.submitAll()
        error_type = self.model.lastError().type()
        error_msg = self.model.lastError().databaseText()

        # Update view
        self.update_model()

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można dodać incydentu.")
            message.exec_()
        else:
            # Clean input
            for child in self.parent().children():
                if child.objectName() in ['comboBox_5', 'comboBox_6', 'comboBox_7']:
                    child.setCurrentIndex(0)

    @pyqtSlot()
    def edit_incident(self):
        error_type = 0
        real_index = self.proxy.mapToSource(self.currentIndex())
        dialog = EditDialog(self.model, self.table, real_index)

        if dialog.exec_():
            self.model.submitAll()
            error_type = self.model.lastError().type()
            error_msg = self.model.lastError().databaseText()

        # Update view
        self.update_model()

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można edytować rekordu.")
            message.exec_()


    @pyqtSlot(bool)
    def delete_incident(self):
        real_index = self.proxy.mapToSource(self.currentIndex())
        real_row = real_index.row()
        if real_row == -1:
            return

        self.model.removeRow(real_row)
        self.model.submitAll()
        error_type = self.model.lastError().type()
        error_msg = self.model.lastError().databaseText()

        # Update view
        self.update_model()

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można usunąć rekordu.")
            message.exec_()
