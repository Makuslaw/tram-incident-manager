from PyQt5.QtSql import QSqlTableModel
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QMessageBox, QHeaderView
from PyQt5.QtCore import pyqtSlot, QRegExp, Qt, QSortFilterProxyModel

from widgets.adddialog import AddDialog
from widgets.database import MyConnection
from widgets.editdialog import EditDialog


class DataTableView(QTableView):
    def __init__(self, parent):
        super(DataTableView, self).__init__(parent)
        self.db = MyConnection.db

        self.model = QSqlTableModel(db=self.db)
        self.proxy = QSortFilterProxyModel(self.model)
        self.table = self.db.tables()[0]

        # TableView settings
        self.setSortingEnabled(True)
        self.verticalHeader().setVisible(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        # TableModel settings
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)

        # ProxyModel settings
        self.proxy.setFilterKeyColumn(-1)

        # TableView signal to slot
        self.doubleClicked.connect(self.edit_record)

        self.set_model(self.table)

    def set_model(self, text):
        self.model.setTable(text)
        self.model.select()
        self.proxy.setSourceModel(self.model)
        self.setModel(self.proxy)

    @pyqtSlot(int)
    def update_model(self, i=1000):
        if i not in [0, 1000]:
            return
        self.set_model(self.table)

    @pyqtSlot(str)
    def change_model(self, text):
        self.table = text
        self.set_model(self.table)

    @pyqtSlot(str)
    def filter_changed(self, text):
        sender = self.sender().objectName()
        if sender == 'lineEdit':
            search = QRegExp(text, cs=Qt.CaseInsensitive)
            self.proxy.setFilterRegExp(search)
        elif sender == 'comboBox_2':
            self.proxy.setFilterKeyColumn(int(text))

    @pyqtSlot(bool)
    def delete_record(self):

        real_index = self.proxy.mapToSource(self.currentIndex())
        real_row = real_index.row()
        if real_row == -1:
            return

        self.model.removeRow(real_row)
        self.model.submitAll()
        error_type = self.model.lastError().type()
        error_msg = self.model.lastError().databaseText()

        # Update view
        self.set_model(self.table)

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można usunąć rekordu.")
            message.exec_()

    @pyqtSlot()
    def edit_record(self):
        error_type = 0
        real_index = self.proxy.mapToSource(self.currentIndex())
        dialog = EditDialog(self.model, self.table, real_index)

        if dialog.exec_():
            self.model.submitAll()
            error_type = self.model.lastError().type()
            error_msg = self.model.lastError().databaseText()

        # Update view
        self.set_model(self.table)

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można edytować rekordu.")
            message.exec_()

    @pyqtSlot()
    def add_record(self):
        if self.table in ['USTERKA', 'PRZYDZIAL']:
            message = QMessageBox()
            message.setText("Użyj odpowiedniego ekranu by dodać.")
            message.exec_()
            return

        error_type = 0
        dialog = AddDialog(self.model, self.table)

        if dialog.exec_():
            self.model.submitAll()
            error_type = self.model.lastError().type()
            error_msg = self.model.lastError().databaseText()
            print(error_msg)

        # Update view
        self.set_model(self.table)

        # Display error message
        if error_type != 0:
            message = QMessageBox()
            message.setText("Nie można dodać rekordu.")
            message.exec_()


