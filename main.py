from PyQt5.QtWidgets import QApplication

from widgets.mainwindow import MainWindow


app = QApplication([])
window = MainWindow()
window.show()
app.exec_()
